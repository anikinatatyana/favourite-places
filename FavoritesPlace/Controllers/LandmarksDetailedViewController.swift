//
//  LandmarksDetailedViewController.swift
//  FavoritesPlace
//
//  Created by Tatyana Anikina on 21.01.2021.
//

import UIKit
import CoreLocation
import MapKit

class LandmarksDetailedViewController: UIViewController {

    var dataSorted = NameData.init(name: "", isFavorite: false, imageName: "", park: "", state: "", coordinates: Coordinates(longitude: 0, latitude: 0))

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var buttonWithImage: UIButton!
    @IBOutlet weak var viewForSchadow: UIView!
    @IBOutlet weak var parkLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var starLabel: UIImageView!
    @IBOutlet weak var mapView: MKMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureButtonAndView()
        configureLabel()

        let turtleRock = MKPointAnnotation()
        turtleRock.title = "TurtleRock"
        turtleRock.coordinate = CLLocationCoordinate2D(latitude: dataSorted.coordinates.latitude, longitude: dataSorted.coordinates.longitude)

        centerMapOnLocation(turtleRock.coordinate, mapView: mapView)
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }

        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)

        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }

        return annotationView
    }

    func centerMapOnLocation(_ center: CLLocationCoordinate2D, mapView: MKMapView) {
        let regionRadius: CLLocationDistance = 10000
        let coordinateRegion = MKCoordinateRegion(center: center, latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }

    private func configureButtonAndView() {
        buttonWithImage.setImage(UIImage(named: dataSorted.imageName), for: .normal)
        buttonWithImage.layer.cornerRadius = buttonWithImage.frame.width / 2
        buttonWithImage.layer.borderWidth = 5
        buttonWithImage.layer.borderColor = CGColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)

        viewForSchadow.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.45).cgColor
        viewForSchadow.layer.cornerRadius = viewForSchadow.frame.width / 2
        viewForSchadow.layer.shadowOffset = .zero
        viewForSchadow.layer.shadowOpacity = 1.0
        viewForSchadow.layer.masksToBounds = false
        viewForSchadow.layer.shadowRadius = 6
    
    }

    private func configureLabel() {
        nameLabel.text = dataSorted.name
        parkLabel.text = dataSorted.park
        stateLabel.text = dataSorted.state

        if dataSorted.isFavorite {
            starLabel.image = UIImage(systemName: "star.fill")
        } else {
            starLabel.image = UIImage(systemName: "star")
        }
        
    }

}
