//
//  coordinates.swift
//  FavoritesPlace
//
//  Created by Tatyana Anikina on 24.01.2021.
//

import Foundation

struct Coordinates: Codable {
    
    var longitude: Double
    var latitude: Double
}
