//
//  FavoriteLandmarksTableViewCell.swift
//  FavoritesPlace
//
//  Created by Tatyana Anikina on 21.01.2021.
//

import UIKit

protocol FavoriteLandmarksTableViewCellDelegate : class {
    func didChangeSwitchState(_: FavoriteLandmarksTableViewCell, isOn: Bool)
}

class FavoriteLandmarksTableViewCell: UITableViewCell {

    @IBOutlet weak var switchFavorites: UISwitch!

    weak var delegate: FavoriteLandmarksTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        switchFavorites.isOn = false
    }

    @IBAction func switchAction(_ sender: UISwitch) {
        self.delegate?.didChangeSwitchState(self, isOn: switchFavorites.isOn)

    }
}


