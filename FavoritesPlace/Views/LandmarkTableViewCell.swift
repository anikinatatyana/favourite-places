//
//  LandmarkTableViewCell.swift
//  FavoritesPlace
//
//  Created by Tatyana Anikina on 21.01.2021.
//

import UIKit

class LandmarkTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imagePlace: UIImageView!
    @IBOutlet weak var imageStar: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()

    }

}
